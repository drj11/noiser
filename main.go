package main

import (
	"flag"
	"fmt"
	"math"
	"math/rand"
)

var kp = flag.Float64("k", 0.5, "autoregression coefficient")

func main() {
	flag.Parse()
	r := rand.New(rand.NewSource(rand.Int63()))
	rnd := NewARRand(r, *kp)

	var m [50][80]int

	for y := 0; y < 50; y++ {
		for x := 0; x < 80; x++ {
			m[y][x] = rnd.Intn(256)
		}
	}

	for y := 0; y < 50; y++ {
		for x := 0; x < 80; x++ {
			l := m[y][x]
			fmt.Printf("%s/", ANSIGrey(255, l))
		}
		fmt.Println("\033[m")
	}
}

// return an ANSI colour control string for the foreground and
// background grey levels (0 to 255) given.
func ANSIGrey(background, foreground int) string {
	return fmt.Sprintf("\033[48;2;%d;%d;%dm\033[38;2;%d;%d;%dm",
		background, background, background,
		foreground, foreground, foreground,
	)
}

type ARRand struct {
	Rand *rand.Rand
	// coefficient of Autoregression
	k float64
	// previous result of Rand.Float64()
	p float64
}

func (r *ARRand) Float64() float64 {
	x := r.Rand.Float64()
	r.p = r.k*r.p + (1-r.k)*x
	return r.p
}

func (r *ARRand) Intn(n int) int {
	return int(math.Floor(r.Float64() * float64(n)))
}

func NewARRand(r *rand.Rand, k float64) ARRand {
	return ARRand{
		r, k, r.Float64(),
	}
}

#!/usr/bin/env python

import random
import sys

def stripes(out):
    """Print out the code for some SVG."""

    out.write("""<svg version="1.1"
width="800" height="600"
xmlns="http://www.w3.org/2000/svg">
""")
    out.write("""<style>
.K {
  fill: black;
}
.C {
  fill: cyan;
  opacity: 0.6;
}
.M {
  fill: magenta;
  opacity: 0.8;
}
.Y {
  fill: yellow;
  opacity: 0.4;
}
</style>
""")
    slice_board(out, "K", 37.5)
    slice_board(out, "M", 67.5)
    slice_board(out, "C", 07.5)
    slice_board(out, "Y", 82.5)

    out.write("</svg>")

def slice_board(out, classname, angle):
    """To `out` write an SVG group, using
    classname and angle.
    """
    out.write(f"""<g class="{classname}" transform="rotate({angle} 400 300)">\n""")
    for y in range(-960, 960, 12):
        y1 = y+12
        xs = gen_xs()
        for i in range(0, len(xs), 2):
            x0, x1 = xs[i:i+2]
            out.write(f"""<path d="M {x0} {y} H {x1} V {y1} H {x0} Z" />\n""")
    out.write("</g>")

def gen_xs():
    """Generate a sequence of increasing xs."""
    shape, scale = 2, 12
    xs = []
    # start sample one variate in. buggy.
    x = -random.gammavariate(shape, scale)
    for i in range(60):
        x += random.gammavariate(shape, scale)
        xs.append(x)
    return xs

if __name__ == "__main__":
    with open("out.svg", "w") as w:
        stripes(w)
